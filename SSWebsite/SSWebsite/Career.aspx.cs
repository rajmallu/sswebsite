﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace SSWebsite
{
    public partial class Career : System.Web.UI.Page
    {
        string jobID;
        string jobTITLE;
        string jobLOCATION;
        string jobSUMMARY;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                SqlConnection con = new SqlConnection("Data Source=rsl-server1.database.windows.net;Initial Catalog=SynapteinSolutionsDB;MultipleActiveResultSets=True;Persist Security Info=True;User ID=SQL-Admin;Password=Sp33dy0425!");
                string allJobs = "SELECT job_id,title,location,summary,required_skills,responsibilities FROM Job where status = 'open'";
                con.Open();
                SqlCommand cmd = new SqlCommand(allJobs, con);

                SqlDataReader dataReader = cmd.ExecuteReader();

                int count = 0;

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        jobID = dataReader.GetString(0);//
                        count += 1;

                        // li
                        HtmlGenericControl li = new HtmlGenericControl("li");
                        jobsList.Controls.Add(li);

                        // h3
                        HtmlGenericControl h3_title = new HtmlGenericControl("h3");
                        h3_title.Attributes.Add("class", "btm_spaceNone");
                        li.Controls.Add(h3_title);

                        HtmlGenericControl a_title = new HtmlGenericControl("a");
                        a_title.Attributes.Add("aria-controls", "collapseOne");
                        a_title.Attributes.Add("aria-expanded", "true");
                        a_title.Attributes.Add("class", "page-scroll");
                        string idValue = "course" + count;
                        a_title.Attributes.Add("data-parent", "#" + idValue);
                        a_title.Attributes.Add("data-toggle", "collapse");
                        a_title.Attributes.Add("href", "#" + idValue);

                        a_title.Attributes.Add("role", "button");
                        jobTITLE = dataReader.GetString(1);//
                        a_title.InnerText = jobTITLE;
                        h3_title.Controls.Add(a_title);

                        // div
                        HtmlGenericControl div = new HtmlGenericControl("div");
                        div.Attributes.Add("class", "collapse");
                        div.Attributes.Add("id", idValue);
                        idValue = "";
                        li.Controls.Add(div);

                        // p
                        HtmlGenericControl p_location = new HtmlGenericControl("p");
                        jobLOCATION = dataReader.GetString(2);//
                        p_location.InnerText = "Location: " + jobLOCATION;//
                        div.Controls.Add(p_location);

                        // h4
                        HtmlGenericControl h4_summary_title = new HtmlGenericControl("h4");
                        h4_summary_title.Attributes.Add("class", "text_bold");
                        h4_summary_title.InnerText = "Summary:";
                        div.Controls.Add(h4_summary_title);

                        // p
                        HtmlGenericControl p_summary = new HtmlGenericControl("p");
                        jobSUMMARY = dataReader.GetString(3);
                        p_summary.InnerText = jobSUMMARY;//

                        h4_summary_title.Controls.Add(p_summary);


                        // h4
                        HtmlGenericControl h4_responsibilities_title = new HtmlGenericControl("h4");
                        h4_responsibilities_title.Attributes.Add("class", "text_bold");
                        h4_responsibilities_title.InnerText = "Responsibilities:";
                        div.Controls.Add(h4_responsibilities_title);

                        // ul
                        HtmlGenericControl ul_responsibilities = new HtmlGenericControl("ul");
                        h4_responsibilities_title.Controls.Add(ul_responsibilities);

                        // li
                        string db_responsibilities = dataReader.GetString(5);//
                        string[] resp = db_responsibilities.Split('^');
                        foreach (string str in resp)
                        {
                            HtmlGenericControl li_responsibilities = new HtmlGenericControl("li");
                            li_responsibilities.Style.Add("font-weight", "normal");
                            li_responsibilities.InnerText = str;
                            ul_responsibilities.Controls.Add(li_responsibilities);
                        }

                        /* LOOP HERE TO PUT RESPONSIBILITIES 
                        try
                        {
                            string responsibilitiesQuery = "SELECT content FROM Resp WHERE responsibility_id='" + jobID+"';";
                            SqlCommand respCmd = new SqlCommand(responsibilitiesQuery, con);

                            SqlDataReader respDR = respCmd.ExecuteReader();

                            if (respDR.HasRows)
                            {
                                while (respDR.Read())
                                {
                                    HtmlGenericControl li_responsibilities = new HtmlGenericControl("li");
                                    li_responsibilities.InnerText = respDR.GetString(0);
                                    ul_responsibilities.Controls.Add(li_responsibilities);

                                }
                            }

                        }
                        catch(Exception exx)
                        {
                            HtmlGenericControl li_responsibilities = new HtmlGenericControl("li");
                            li_responsibilities.InnerText = "Unable to print responsibilities: "+exx.Message;
                            ul_responsibilities.Controls.Add(li_responsibilities);
                        }
                        */



                        // h4
                        HtmlGenericControl h4_requiredskills_title = new HtmlGenericControl("h4");
                        h4_requiredskills_title.Attributes.Add("class", "text_bold");
                        h4_requiredskills_title.InnerText = "Required Skills:";
                        div.Controls.Add(h4_requiredskills_title);

                        // p
                        HtmlGenericControl p_requiredskills = new HtmlGenericControl("p");
                        p_requiredskills.InnerText = dataReader.GetString(4);//

                        h4_requiredskills_title.Controls.Add(p_requiredskills);

                        // a
                        HtmlGenericControl a_applynow = new HtmlGenericControl("a");
                        a_applynow.Attributes.Add("class", "btn btn-primary btn-color");
                        /* SEND SOME DATA FOR NEXT PAGE */
                        a_applynow.Attributes.Add("href", "/ApplyPosition.aspx?j_id="+jobID+"&j_title="+jobTITLE+"&j_location="+jobLOCATION+"&j_summary="+jobSUMMARY);//field1=value1&field2=value2
                        a_applynow.Attributes.Add("target", "_blank");
                        a_applynow.InnerText = "Apply Now";

                        div.Controls.Add(a_applynow);
                    }
                }
                con.Close();
            } // try
            catch (Exception ex)
            {
                lblMessage.Text = "Internal Error! " + ex.Message;
            }
        }//page load

        //public string JOBID
        //{
        //    get
        //    {
        //        return jobID;
        //    }
        //}
        //public string JOBTITLE{get{return jobTITLE;}}
        
        //public string JOBLOCATION { get { return jobLOCATION; } }

        //public string JOBSUMMARY { get { return jobSUMMARY; } }

    }//class
}//namespace