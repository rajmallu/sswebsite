﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SSWebsite
{
    public partial class ContactUs : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["reason"] != null)
            {
                reason.Text = Request.QueryString["reason"];
            }
        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            string user_name = name.Text;
            string user_email = email.Text;
            string user_phone = Phone.Text;
            string user_reason = reason.Text;
            string user_comment = comment.Text;

            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
            mail.To.Add("rajamohan7252@gmail.com");
            mail.From = new MailAddress("rajmallu@outlook.com", "Email head", System.Text.Encoding.UTF8);
            mail.Subject = user_reason;
            mail.SubjectEncoding = System.Text.Encoding.UTF8;
            mail.Body = "<h3>Feedback from Synaptein Solutions website</h3><br>"+
                            "Name: <strong>"+user_name+
                            "</strong><br>Email: <strong>" + user_email+
                            "</strong><br>Phone: <strong>" + user_phone+
                            "</strong><br>Reason: <strong>" + user_reason+
                            "</strong><br>Content: <br>" + user_comment+
                            "<br><br><a href='mailto:"+user_email+"'>Reply to User</a>";
            mail.BodyEncoding = System.Text.Encoding.UTF8;
            mail.IsBodyHtml = true;
            mail.Priority = System.Net.Mail.MailPriority.High;
            SmtpClient client = new SmtpClient();
            client.Credentials = new System.Net.NetworkCredential("rajmallu@outlook.com", "raj051117");
            client.Port = 587;
            client.Host = "smtp.outlook.com";
            client.EnableSsl = true;
            try
            {
                client.Send(mail);
                lblMessage.Text = "Feedback Submitted.";
                name.Text = email.Text = Phone.Text = comment.Text = "";
                reason.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                lblMessage.Text = "Unable to send feedback. Please try later!!";
            }
        }
    }
}