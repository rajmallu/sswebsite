﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Profiles.aspx.cs" Inherits="SSWebsite.Profiles" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Synaptein Solutions</title>
    <link href="css/mdb.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="/css/style.css" rel="stylesheet" />

    <style>
        #ib00l {border: 3px solid white;background-color: none;}
        #ib00l:hover {border: 3px solid #0077B5;background-color: #0077B5;}
        #ib00m{border: 3px solid #ffa500;background-color: none;}
        #ib00m:hover{border: 3px solid maroon;background-color: maroon;}

        #ib01l {border: 3px solid white;background-color: none;}
        #ib01l:hover {border: 3px solid #0077B5;background-color: #0077B5;}
        #ib01m{border: 3px solid #ffa500;background-color: none;}
        #ib01m:hover{border: 3px solid maroon;background-color: maroon;}

        #ib02l {border: 3px solid white;background-color: none;}
        #ib02l:hover {border: 3px solid #0077B5;background-color: #0077B5;}
        #ib02m{border: 3px solid #ffa500;background-color: none;}
        #ib02m:hover{border: 3px solid maroon;background-color: maroon;}

        #ib03l {border: 3px solid white;background-color: none;}
        #ib03l:hover {border: 3px solid #0077B5;background-color: #0077B5;}
        #ib03m{border: 3px solid #ffa500;background-color: none;}
        #ib03m:hover{border: 3px solid maroon;background-color: maroon;}

        #ib10l {border: 3px solid white;background-color: none;}
        #ib10l:hover {border: 3px solid #0077B5;background-color: #0077B5;}
        #ib10m{border: 3px solid #ffa500;background-color: none;}
        #ib10m:hover{border: 3px solid maroon;background-color: maroon;}

        #ib11l {border: 3px solid white;background-color: none;}
        #ib11l:hover {border: 3px solid #0077B5;background-color: #0077B5;}
        #ib11m{border: 3px solid #ffa500;background-color: none;}
        #ib11m:hover{border: 3px solid maroon;background-color: maroon;}

        #ib12l {border: 3px solid white;background-color: none;}
        #ib12l:hover {border: 3px solid #0077B5;background-color: #0077B5;}
        #ib12m{border: 3px solid #ffa500;background-color: none;}
        #ib12m:hover{border: 3px solid maroon;background-color: maroon;}

        #ib13l {border: 3px solid white;background-color: none;}
        #ib13l:hover {border: 3px solid #0077B5;background-color: #0077B5;}
        #ib13m{border: 3px solid #ffa500;background-color: none;}
        #ib13m:hover{border: 3px solid maroon;background-color: maroon;}

        #ib20l {border: 3px solid white;background-color: none;}
        #ib20l:hover {border: 3px solid #0077B5;background-color: #0077B5;}
        #ib20m{border: 3px solid #ffa500;background-color: none;}
        #ib20m:hover{border: 3px solid maroon;background-color: maroon;}

        #ib21l {border: 3px solid white;background-color: none;}
        #ib21l:hover {border: 3px solid #0077B5;background-color: #0077B5;}
        #ib21m{border: 3px solid #ffa500;background-color: none;}
        #ib21m:hover{border: 3px solid maroon;background-color: maroon;}

        #ib22l {border: 3px solid white;background-color: none;}
        #ib22l:hover {border: 3px solid #0077B5;background-color: #0077B5;}
        #ib22m{border: 3px solid #ffa500;background-color: none;}
        #ib22m:hover{border: 3px solid maroon;background-color: maroon;}

        #ib23l {border: 3px solid white;background-color: none;}
        #ib23l:hover {border: 3px solid #0077B5;background-color: #0077B5;}
        #ib23m{border: 3px solid #ffa500;background-color: none;}
        #ib23m:hover{border: 3px solid maroon;background-color: maroon;}


    </style>
</head>
<body class="slider" data-spy="scroll" data-target=".navbar" data-offset="" style="background-color: lightgray;">
    <nav class="navbar navbar-light navbar-fixed-top">
        <div class="container-fluid navbar-properties" style="padding-left: 120px; padding-right: 120px; font-family: 'Segoe UI'; background-color: white;">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <img src="/img/Logo.png" style="width: 150px; height: 70px;" />
            </div>
            <div class=" font-weight-bold collapse navbar-collapse navbar-right" id="myNavbar" style="color: black;">
                <ul class="navbar navbar-nav" id="navbar-text">
                    <li class="btn dropdown" style="padding: 23px;">
                        <strong><a href="/index.html" style="color: black; font-size: 1.3em;">Home</a></strong>
                    </li>
                    <li class="btn dropdown" style="padding: 23px;">
                        <strong>
                            <a href="/ContactUs.aspx" style="color: black; font-size: 1.3em;">Contact Us
                                <span class="caret"></span>
                            </a>
                        </strong>
                        <ul class="dropdown-content list-unstyled text-left list-group" style="color: black; padding: 0;">
                            <li class="list-group-item">Certifications</li>
                            <li class="list-group-item">Centers of Excellence</li>
                            <li class="list-group-item">Contract Vehicles</li>
                            <li class="list-group-item">Leadership</li>
                        </ul>
                    </li>
                    <li class="btn dropdown" style="padding: 23px;">
                        <strong>
                            <a href="/services.html" style="color: black; font-size: 1.3em;">Services
                                <span class="caret"></span>
                            </a>
                        </strong>
                        <ul class="dropdown-content list-unstyled text-left list-group" style="color: black; padding: 0;">
                            <li class="list-group-item">Development & Integration</li>
                            <li class="list-group-item">Program & Project Management</li>
                            <li class="list-group-item">>Business Process Consulting</li>
                            <li class="list-group-item">IT Development Support</li>
                            <li class="list-group-item">Business Intelligence and Analytics</li>
                            <li class="list-group-item">JAVA Solutions & Support</li>
                            <li class="list-group-item">Data Management</li>
                            <li class="list-group-item">Information Assurance and Security</li>
                            <li class="list-group-item">Enterprise Risk Management</li>
                        </ul>
                    </li>
                    <li class="btn dropdown" style="padding: 23px;">
                        <strong>
                            <a href="/products.html" style="color: black; font-size: 1.3em;">Products
                                <span class="caret"></span>
                            </a>
                        </strong>
                        <ul class="dropdown-content list-unstyled text-left list-group" style="color: black; padding: 0;">
                            <li class="list-group-item disabled"><strong>Application</strong></li>
                            <li class="list-group-item">Riskspotlight</li>
                            <li class="list-group-item">Apertisoft</li>
                            <li class="list-group-item">ARQ Technology</li>
                            <li class="list-group-item">Purchase Card System</li>
                            <li class="list-group-item disabled"><strong>Mobile</strong></li>
                            <li class="list-group-item">Sitawile</li>
                            <li class="list-group-item">VITwee</li>
                        </ul>
                    </li>
                    <li class="btn dropdown" style="padding: 23px;">
                        <strong>
                            <a href="/partners.html" style="color: black; font-size: 1.3em;">Partners
                                <span class="caret"></span>
                            </a>
                        </strong>
                        <ul class="dropdown-content list-unstyled text-left list-group" style="color: black; padding: 0;">
                            <li class="list-group-item">Microsoft</li>
                            <li class="list-group-item">Redhat</li>
                            <li class="list-group-item">Dell Boomi</li>
                            <li class="list-group-item">rPM3</li>
                            <li class="list-group-item">Riskspotlight</li>
                        </ul>
                    </li>
                    <li class="btn dropdown" style="padding: 23px;">
                        <strong>
                            <a href="/Career.aspx" style="color: black; font-size: 1.3em;">Careers
                                <span class="caret"></span>
                            </a>
                        </strong>
                        <ul class="dropdown-content list-unstyled text-left list-group" style="color: black; padding: 0;">
                            <li class="list-group-item"><a href="/Career.aspx">Current Opening</a></li>
                            <li class="list-group-item"><a href="#">Search Jobs</a></li>
                        </ul>
                    </li>
                    <li class="btn dropdown" style="padding: 23px; color: black; font-size: 1.3em;">
                        <strong>Login
                            <span class="caret"></span>
                        </strong>
                        <ul class="dropdown-content list-unstyled text-left list-group" style="color: black; padding: 0;">
                            <li class="list-group-item"><a href="/Login.aspx">HR Login</a></li>
                            <li class="list-group-item"><a href="https://my.adp.com/static/redbox/login.html" target="_blank">Employee Login</a></li>
                            <li class="list-group-item"><a href="http://www.riskspotlightportal.com/Home/Index" target="_blank">RiskSpotLight Login</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>


    <br />
    <br />
    <br />
    <br />
    <br />
    <div class="container-fluid text-center">
        <div class="well-lg" style="background-image: url('/img/dashboard-bg-bar.jpg')">
            <h2 class="white-text" style="font-family: 'Segoe UI'">Leadership</h2>
            <br />
        </div>
    </div>
    <br />



    <br />

    <br />

    <div class="container-fluid" style="font-family: 'Segoe UI'">
        <form runat="server">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 text-center" style="padding: 3px;">
                        <div class="card view overlay" id="id00">

                            <asp:ImageButton ID="ib00" runat="server" class="card-img-top img-rounded img-fluid" src="/img/PNG/sharad.png" Style="width: 250px; height: 250px;" />
                            <!--
                        <div class="mask flex-center rgba-black-light" style="width: 250px; height: 250px;border-radius: 5px;">
                        &nbsp;
                        </div>
                        -->
                            <div class="card-body">
                                <h4 class="card-title">
                                    <asp:HyperLink ID="hl00" runat="server" data-toggle="collapse" data-target="#id00c" Style="font-family: 'Segoe UI'">Sharad Dayma</asp:HyperLink>
                                </h4>
                                <div class="card-text">
                                    <div id="id00c" class="collapse text-justify">
                                        <h4>CTO</h4>
                                        <div class="text-center">
                                            <asp:ImageButton ID="ib00m" runat="server" class="img-circle" Style="width: 50px; height: 50px;" data-toggle="tooltip" title="Click to send an email!" ImageUrl="img/PNG/msg.png" />
                                            &emsp;<asp:ImageButton ID="ib00l" runat="server" class="img-circle" Style="width: 50px; height: 50px;" ImageUrl="~/img/PNG/linkedin-icon.png" data-toggle="tooltip" title="Click to connect on LinkedIn." PostBackUrl="https://www.linkedin.com" />
                                        </div>
                                        Sharad leads all technology initiatives at RiskSpotlight. His focus is on delivering technology solutions for RiskSpotlight clients and operational processes. He has over 18 years of consulting experience within financial services industry. He has been involved with successful delivery of technology solutions at Securities and Exchange Commission (SEC), Fannie Mae and Freddie Mac. His experience has covered domains such as Mortgage Backed Securities, Credit Default Swaps and Mutual Fund Instruments.
                            He is also CEO and Co-Founder of Synaptein Solutions, a BPM services company based in Virginia, USA.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 text-center" style="padding: 3px;">
                        <div class="card view overlay" id="id01">

                            <asp:ImageButton ID="img01" runat="server" class="card-img-top img-rounded img-fluid" src="/img/PNG/sharad.png" Style="width: 250px; height: 250px;" />
                            <!--
                        <div class="mask flex-center rgba-black-light" style="width: 250px; height: 250px;border-radius: 5px;">
                        &nbsp;
                        </div>
                        -->
                            <div class="card-body">
                                <h4 class="card-title">
                                    <asp:HyperLink ID="hl01" runat="server" data-toggle="collapse" data-target="#id01c" Style="font-family: 'Segoe UI'">Sharad Dayma</asp:HyperLink>
                                </h4>
                                <div class="card-text">
                                    <div id="id01c" class="collapse text-justify">
                                        <h4>CTO</h4>
                                        <div class="text-center">
                                            <asp:ImageButton ID="ib01m" runat="server" class="img-circle" Style="width: 50px; height: 50px;" data-toggle="tooltip" title="Click to send an email!" ImageUrl="img/PNG/msg.png" />
                                            &emsp;<asp:ImageButton ID="ib01l" runat="server" class="img-circle" Style="width: 50px; height: 50px;" ImageUrl="~/img/PNG/linkedin-icon.png" data-toggle="tooltip" title="Click to connect on LinkedIn." PostBackUrl="https://www.linkedin.com" />
                                        </div>
                                        Sharad leads all technology initiatives at RiskSpotlight. His focus is on delivering technology solutions for RiskSpotlight clients and operational processes. He has over 18 years of consulting experience within financial services industry. He has been involved with successful delivery of technology solutions at Securities and Exchange Commission (SEC), Fannie Mae and Freddie Mac. His experience has covered domains such as Mortgage Backed Securities, Credit Default Swaps and Mutual Fund Instruments.
                            He is also CEO and Co-Founder of Synaptein Solutions, a BPM services company based in Virginia, USA.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 text-center" style="padding: 3px;">
                        <div class="card view overlay" id="id02">

                            <asp:ImageButton ID="ib02" runat="server" class="card-img-top img-rounded img-fluid" src="/img/PNG/sharad.png" Style="width: 250px; height: 250px;" />
                            <!--
                        <div class="mask flex-center rgba-black-light" style="width: 250px; height: 250px;border-radius: 5px;">
                        &nbsp;
                        </div>
                        -->
                            <div class="card-body">
                                <h4 class="card-title">
                                    <asp:HyperLink ID="hl02" runat="server" data-toggle="collapse" data-target="#id02c" Style="font-family: 'Segoe UI'">Sharad Dayma</asp:HyperLink>
                                </h4>
                                <div class="card-text">
                                    <div id="id02c" class="collapse text-justify">
                                        <h4>CTO</h4>
                                        <div class="text-center">
                                            <asp:ImageButton ID="ib02m" runat="server" class="img-circle" Style="width: 50px; height: 50px;" data-toggle="tooltip" title="Click to send an email!" ImageUrl="img/PNG/msg.png" />
                                            &emsp;<asp:ImageButton ID="ib02l" runat="server" class="img-circle" Style="width: 50px; height: 50px;" ImageUrl="~/img/PNG/linkedin-icon.png" data-toggle="tooltip" title="Click to connect on LinkedIn." PostBackUrl="https://www.linkedin.com" />
                                        </div>
                                        Sharad leads all technology initiatives at RiskSpotlight. His focus is on delivering technology solutions for RiskSpotlight clients and operational processes. He has over 18 years of consulting experience within financial services industry. He has been involved with successful delivery of technology solutions at Securities and Exchange Commission (SEC), Fannie Mae and Freddie Mac. His experience has covered domains such as Mortgage Backed Securities, Credit Default Swaps and Mutual Fund Instruments.
                            He is also CEO and Co-Founder of Synaptein Solutions, a BPM services company based in Virginia, USA.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 text-center" style="padding: 3px;">
                        <div class="card view overlay" id="id03">

                            <asp:ImageButton ID="ib03" runat="server" class="card-img-top img-rounded img-fluid" src="/img/PNG/sharad.png" Style="width: 250px; height: 250px;" />
                            <!--
                        <div class="mask flex-center rgba-black-light" style="width: 250px; height: 250px;border-radius: 5px;">
                        &nbsp;
                        </div>
                        -->
                            <div class="card-body">
                                <h4 class="card-title">
                                    <asp:HyperLink ID="hl03" runat="server" data-toggle="collapse" data-target="#id03c" Style="font-family: 'Segoe UI'">Sharad Dayma</asp:HyperLink>
                                </h4>
                                <div class="card-text">
                                    <div id="id03c" class="collapse text-justify">
                                        <h4>CTO</h4>
                                        <div class="text-center">
                                            <asp:ImageButton ID="ib03m" runat="server" class="img-circle" Style="width: 50px; height: 50px;" data-toggle="tooltip" title="Click to send an email!" ImageUrl="img/PNG/msg.png" />
                                            &emsp;<asp:ImageButton ID="ib03l" runat="server" class="img-circle" Style="width: 50px; height: 50px;" ImageUrl="~/img/PNG/linkedin-icon.png" data-toggle="tooltip" title="Click to connect on LinkedIn." PostBackUrl="https://www.linkedin.com" />
                                        </div>
                                        Sharad leads all technology initiatives at RiskSpotlight. His focus is on delivering technology solutions for RiskSpotlight clients and operational processes. He has over 18 years of consulting experience within financial services industry. He has been involved with successful delivery of technology solutions at Securities and Exchange Commission (SEC), Fannie Mae and Freddie Mac. His experience has covered domains such as Mortgage Backed Securities, Credit Default Swaps and Mutual Fund Instruments.
                            He is also CEO and Co-Founder of Synaptein Solutions, a BPM services company based in Virginia, USA.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 text-center" style="padding: 3px;">
                        <div class="card view overlay" id="id10">

                            <asp:ImageButton ID="ib10" runat="server" class="card-img-top img-rounded img-fluid" src="/img/PNG/sharad.png" Style="width: 250px; height: 250px;" />
                            <!--
                        <div class="mask flex-center rgba-black-light" style="width: 250px; height: 250px;border-radius: 5px;">
                        &nbsp;
                        </div>
                        -->
                            <div class="card-body">
                                <h4 class="card-title">
                                    <asp:HyperLink ID="hl10" runat="server" data-toggle="collapse" data-target="#id10c" Style="font-family: 'Segoe UI'">Sharad Dayma</asp:HyperLink>
                                </h4>
                                <div class="card-text">
                                    <div id="id10c" class="collapse text-justify">
                                        <h4>CTO</h4>
                                        <div class="text-center">
                                            <asp:ImageButton ID="ib10m" runat="server" class="img-circle" Style="width: 50px; height: 50px;" data-toggle="tooltip" title="Click to send an email!" ImageUrl="img/PNG/msg.png" />
                                            &emsp;<asp:ImageButton ID="ib10l" runat="server" class="img-circle" Style="width: 50px; height: 50px;" ImageUrl="~/img/PNG/linkedin-icon.png" data-toggle="tooltip" title="Click to connect on LinkedIn." PostBackUrl="https://www.linkedin.com" />
                                        </div>
                                        Sharad leads all technology initiatives at RiskSpotlight. His focus is on delivering technology solutions for RiskSpotlight clients and operational processes. He has over 18 years of consulting experience within financial services industry. He has been involved with successful delivery of technology solutions at Securities and Exchange Commission (SEC), Fannie Mae and Freddie Mac. His experience has covered domains such as Mortgage Backed Securities, Credit Default Swaps and Mutual Fund Instruments.
                            He is also CEO and Co-Founder of Synaptein Solutions, a BPM services company based in Virginia, USA.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 text-center" style="padding: 3px;">
                        <div class="card view overlay" id="id11">

                            <asp:ImageButton ID="ib11" runat="server" class="card-img-top img-rounded img-fluid" src="/img/PNG/sharad.png" Style="width: 250px; height: 250px;" />
                            <!--
                        <div class="mask flex-center rgba-black-light" style="width: 250px; height: 250px;border-radius: 5px;">
                        &nbsp;
                        </div>
                        -->
                            <div class="card-body">
                                <h4 class="card-title">
                                    <asp:HyperLink ID="hl11" runat="server" data-toggle="collapse" data-target="#id11c" Style="font-family: 'Segoe UI'">Sharad Dayma</asp:HyperLink>
                                </h4>
                                <div class="card-text">
                                    <div id="id11c" class="collapse text-justify">
                                        <h4>CTO</h4>
                                        <div class="text-center">
                                            <asp:ImageButton ID="ib11m" runat="server" class="img-circle" Style="width: 50px; height: 50px;" data-toggle="tooltip" title="Click to send an email!" ImageUrl="img/PNG/msg.png" />
                                            &emsp;<asp:ImageButton ID="ib11l" runat="server" class="img-circle" Style="width: 50px; height: 50px;" ImageUrl="~/img/PNG/linkedin-icon.png" data-toggle="tooltip" title="Click to connect on LinkedIn." PostBackUrl="https://www.linkedin.com" />
                                        </div>
                                        Sharad leads all technology initiatives at RiskSpotlight. His focus is on delivering technology solutions for RiskSpotlight clients and operational processes. He has over 18 years of consulting experience within financial services industry. He has been involved with successful delivery of technology solutions at Securities and Exchange Commission (SEC), Fannie Mae and Freddie Mac. His experience has covered domains such as Mortgage Backed Securities, Credit Default Swaps and Mutual Fund Instruments.
                            He is also CEO and Co-Founder of Synaptein Solutions, a BPM services company based in Virginia, USA.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 text-center" style="padding: 3px;">
                        <div class="card view overlay" id="id12">

                            <asp:ImageButton ID="ib12" runat="server" class="card-img-top img-rounded img-fluid" src="/img/PNG/sharad.png" Style="width: 250px; height: 250px;" />
                            <!--
                        <div class="mask flex-center rgba-black-light" style="width: 250px; height: 250px;border-radius: 5px;">
                        &nbsp;
                        </div>
                        -->
                            <div class="card-body">
                                <h4 class="card-title">
                                    <asp:HyperLink ID="hl12" runat="server" data-toggle="collapse" data-target="#id12c" Style="font-family: 'Segoe UI'">Sharad Dayma</asp:HyperLink>
                                </h4>
                                <div class="card-text">
                                    <div id="id12c" class="collapse text-justify">
                                        <h4>CTO</h4>
                                        <div class="text-center">
                                            <asp:ImageButton ID="ib12m" runat="server" class="img-circle" Style="width: 50px; height: 50px;" data-toggle="tooltip" title="Click to send an email!" ImageUrl="img/PNG/msg.png" />
                                            &emsp;<asp:ImageButton ID="ib12l" runat="server" class="img-circle" Style="width: 50px; height: 50px;" ImageUrl="~/img/PNG/linkedin-icon.png" data-toggle="tooltip" title="Click to connect on LinkedIn." PostBackUrl="https://www.linkedin.com" />
                                        </div>
                                        Sharad leads all technology initiatives at RiskSpotlight. His focus is on delivering technology solutions for RiskSpotlight clients and operational processes. He has over 18 years of consulting experience within financial services industry. He has been involved with successful delivery of technology solutions at Securities and Exchange Commission (SEC), Fannie Mae and Freddie Mac. His experience has covered domains such as Mortgage Backed Securities, Credit Default Swaps and Mutual Fund Instruments.
                            He is also CEO and Co-Founder of Synaptein Solutions, a BPM services company based in Virginia, USA.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 text-center" style="padding: 3px;">
                        <div class="card view overlay" id="id13">

                            <asp:ImageButton ID="ib13" runat="server" class="card-img-top img-rounded img-fluid" src="/img/PNG/sharad.png" Style="width: 250px; height: 250px;" />
                            <!--
                        <div class="mask flex-center rgba-black-light" style="width: 250px; height: 250px;border-radius: 5px;">
                        &nbsp;
                        </div>
                        -->
                            <div class="card-body">
                                <h4 class="card-title">
                                    <asp:HyperLink ID="hl13" runat="server" data-toggle="collapse" data-target="#id13c" Style="font-family: 'Segoe UI'">Sharad Dayma</asp:HyperLink>
                                </h4>
                                <div class="card-text">
                                    <div id="id13c" class="collapse text-justify">
                                        <h4>CTO</h4>
                                        <div class="text-center">
                                            <asp:ImageButton ID="ib13m" runat="server" class="img-circle" Style="width: 50px; height: 50px;" data-toggle="tooltip" title="Click to send an email!" ImageUrl="img/PNG/msg.png" />
                                            &emsp;<asp:ImageButton ID="ib13l" runat="server" class="img-circle" Style="width: 50px; height: 50px;" ImageUrl="~/img/PNG/linkedin-icon.png" data-toggle="tooltip" title="Click to connect on LinkedIn." PostBackUrl="https://www.linkedin.com" />
                                        </div>
                                        Sharad leads all technology initiatives at RiskSpotlight. His focus is on delivering technology solutions for RiskSpotlight clients and operational processes. He has over 18 years of consulting experience within financial services industry. He has been involved with successful delivery of technology solutions at Securities and Exchange Commission (SEC), Fannie Mae and Freddie Mac. His experience has covered domains such as Mortgage Backed Securities, Credit Default Swaps and Mutual Fund Instruments.
                            He is also CEO and Co-Founder of Synaptein Solutions, a BPM services company based in Virginia, USA.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 text-center" style="padding: 3px;">
                        <div class="card view overlay" id="id20">

                            <asp:ImageButton ID="ib20" runat="server" class="card-img-top img-rounded img-fluid" src="/img/PNG/sharad.png" Style="width: 250px; height: 250px;" />
                            <!--
                        <div class="mask flex-center rgba-black-light" style="width: 250px; height: 250px;border-radius: 5px;">
                        &nbsp;
                        </div>
                        -->
                            <div class="card-body">
                                <h4 class="card-title">
                                    <asp:HyperLink ID="hl20" runat="server" data-toggle="collapse" data-target="#id20c" Style="font-family: 'Segoe UI'">Sharad Dayma</asp:HyperLink>
                                </h4>
                                <div class="card-text">
                                    <div id="id20c" class="collapse text-justify">
                                        <h4>CTO</h4>
                                        <div class="text-center">
                                            <asp:ImageButton ID="ib20m" runat="server" class="img-circle" Style="width: 50px; height: 50px;" data-toggle="tooltip" title="Click to send an email!" ImageUrl="img/PNG/msg.png" />
                                            &emsp;<asp:ImageButton ID="ib20l" runat="server" class="img-circle" Style="width: 50px; height: 50px;" ImageUrl="~/img/PNG/linkedin-icon.png" data-toggle="tooltip" title="Click to connect on LinkedIn." PostBackUrl="https://www.linkedin.com" />
                                        </div>
                                        Sharad leads all technology initiatives at RiskSpotlight. His focus is on delivering technology solutions for RiskSpotlight clients and operational processes. He has over 18 years of consulting experience within financial services industry. He has been involved with successful delivery of technology solutions at Securities and Exchange Commission (SEC), Fannie Mae and Freddie Mac. His experience has covered domains such as Mortgage Backed Securities, Credit Default Swaps and Mutual Fund Instruments.
                            He is also CEO and Co-Founder of Synaptein Solutions, a BPM services company based in Virginia, USA.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 text-center" style="padding: 3px;">
                        <div class="card view overlay" id="id21">

                            <asp:ImageButton ID="ib21" runat="server" class="card-img-top img-rounded img-fluid" src="/img/PNG/sharad.png" Style="width: 250px; height: 250px;" />
                            <!--
                        <div class="mask flex-center rgba-black-light" style="width: 250px; height: 250px;border-radius: 5px;">
                        &nbsp;
                        </div>
                        -->
                            <div class="card-body">
                                <h4 class="card-title">
                                    <asp:HyperLink ID="hl21" runat="server" data-toggle="collapse" data-target="#id21c" Style="font-family: 'Segoe UI'">Sharad Dayma</asp:HyperLink>
                                </h4>
                                <div class="card-text">
                                    <div id="id21c" class="collapse text-justify">
                                        <h4>CTO</h4>
                                        <div class="text-center">
                                            <asp:ImageButton ID="ib21m" runat="server" class="img-circle" Style="width: 50px; height: 50px;" data-toggle="tooltip" title="Click to send an email!" ImageUrl="img/PNG/msg.png" />
                                            &emsp;<asp:ImageButton ID="ib21l" runat="server" class="img-circle" Style="width: 50px; height: 50px;" ImageUrl="~/img/PNG/linkedin-icon.png" data-toggle="tooltip" title="Click to connect on LinkedIn." PostBackUrl="https://www.linkedin.com" />
                                        </div>
                                        Sharad leads all technology initiatives at RiskSpotlight. His focus is on delivering technology solutions for RiskSpotlight clients and operational processes. He has over 18 years of consulting experience within financial services industry. He has been involved with successful delivery of technology solutions at Securities and Exchange Commission (SEC), Fannie Mae and Freddie Mac. His experience has covered domains such as Mortgage Backed Securities, Credit Default Swaps and Mutual Fund Instruments.
                            He is also CEO and Co-Founder of Synaptein Solutions, a BPM services company based in Virginia, USA.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 text-center" style="padding: 3px;">
                        <div class="card view overlay" id="id22">

                            <asp:ImageButton ID="ib22" runat="server" class="card-img-top img-rounded img-fluid" src="/img/PNG/sharad.png" Style="width: 250px; height: 250px;" />
                            <!--
                        <div class="mask flex-center rgba-black-light" style="width: 250px; height: 250px;border-radius: 5px;">
                        &nbsp;
                        </div>
                        -->
                            <div class="card-body">
                                <h4 class="card-title">
                                    <asp:HyperLink ID="hl22" runat="server" data-toggle="collapse" data-target="#id22c" Style="font-family: 'Segoe UI'">Sharad Dayma</asp:HyperLink>
                                </h4>
                                <div class="card-text">
                                    <div id="id22c" class="collapse text-justify">
                                        <h4>CTO</h4>
                                        <div class="text-center">
                                            <asp:ImageButton ID="ib22m" runat="server" class="img-circle" Style="width: 50px; height: 50px;" data-toggle="tooltip" title="Click to send an email!" ImageUrl="img/PNG/msg.png" />
                                            &emsp;<asp:ImageButton ID="ib22l" runat="server" class="img-circle" Style="width: 50px; height: 50px;" ImageUrl="~/img/PNG/linkedin-icon.png" data-toggle="tooltip" title="Click to connect on LinkedIn." PostBackUrl="https://www.linkedin.com" />
                                        </div>
                                        Sharad leads all technology initiatives at RiskSpotlight. His focus is on delivering technology solutions for RiskSpotlight clients and operational processes. He has over 18 years of consulting experience within financial services industry. He has been involved with successful delivery of technology solutions at Securities and Exchange Commission (SEC), Fannie Mae and Freddie Mac. His experience has covered domains such as Mortgage Backed Securities, Credit Default Swaps and Mutual Fund Instruments.
                            He is also CEO and Co-Founder of Synaptein Solutions, a BPM services company based in Virginia, USA.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 text-center" style="padding: 3px;">
                        <div class="card view overlay" id="id23">

                            <asp:ImageButton ID="ib23" runat="server" class="card-img-top img-rounded img-fluid" src="/img/PNG/sharad.png" Style="width: 250px; height: 250px;" />
                            <!--
                        <div class="mask flex-center rgba-black-light" style="width: 250px; height: 250px;border-radius: 5px;">
                        &nbsp;
                        </div>
                        -->
                            <div class="card-body">
                                <h4 class="card-title">
                                    <asp:HyperLink ID="hl23" runat="server" data-toggle="collapse" data-target="#id23c" Style="font-family: 'Segoe UI'">Sharad Dayma</asp:HyperLink>
                                </h4>
                                <div class="card-text">
                                    <div id="id23c" class="collapse text-justify">
                                        <h4>CTO</h4>
                                        <div class="text-center">
                                            <asp:ImageButton ID="ib23m" runat="server" class="img-circle" Style="width: 50px; height: 50px;" data-toggle="tooltip" title="Click to send an email!" ImageUrl="img/PNG/msg.png" />
                                            &emsp;<asp:ImageButton ID="ib23l" runat="server" class="img-circle" Style="width: 50px; height: 50px;" ImageUrl="~/img/PNG/linkedin-icon.png" data-toggle="tooltip" title="Click to connect on LinkedIn." PostBackUrl="https://www.linkedin.com" />
                                        </div>
                                        Sharad leads all technology initiatives at RiskSpotlight. His focus is on delivering technology solutions for RiskSpotlight clients and operational processes. He has over 18 years of consulting experience within financial services industry. He has been involved with successful delivery of technology solutions at Securities and Exchange Commission (SEC), Fannie Mae and Freddie Mac. His experience has covered domains such as Mortgage Backed Securities, Credit Default Swaps and Mutual Fund Instruments.
                            He is also CEO and Co-Founder of Synaptein Solutions, a BPM services company based in Virginia, USA.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </form>
    </div>
    <br />
    <br />


    <div id="footer">
        <div class="container">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-8 col-sm-6 col-xs-6 footer-custom">Copyrights©Synaptein Solutions 2015.All rights reserved</div>
                <div class="col-md-4 col-sm-6 text-right left-wrapper">
                    <!--      <div class="col-md-6 hidden-sm hidden-xs"> &nbsp;</div>-->

                    <a href="#">
                        <img src="img/facebook.png" />
                    </a>
                    <a href="#">
                        <img src="img/twitter.png" />
                    </a>
                    <a href="#">
                        <img src="img/linkedin.png" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('[data-toggle="popover"]').popover();

            $("#id00").hover(function () { $('#id00c').collapse('show'); }, function () { $('#id00c').collapse('hide'); });
            $("#id01").hover(function () { $('#id01c').collapse('show'); }, function () { $('#id01c').collapse('hide'); });
            $("#id02").hover(function () { $('#id02c').collapse('show'); }, function () { $('#id02c').collapse('hide'); });
            $("#id03").hover(function () { $('#id03c').collapse('show'); }, function () { $('#id03c').collapse('hide'); });

            $("#id10").hover(function () { $('#id10c').collapse('show'); }, function () { $('#id10c').collapse('hide'); });
            $("#id11").hover(function () { $('#id11c').collapse('show'); }, function () { $('#id11c').collapse('hide'); });
            $("#id12").hover(function () { $('#id12c').collapse('show'); }, function () { $('#id12c').collapse('hide'); });
            $("#id13").hover(function () { $('#id13c').collapse('show'); }, function () { $('#id13c').collapse('hide'); });

            $("#id20").hover(function () { $('#id20c').collapse('show'); }, function () { $('#id20c').collapse('hide'); });
            $("#id21").hover(function () { $('#id21c').collapse('show'); }, function () { $('#id21c').collapse('hide'); });
            $("#id22").hover(function () { $('#id22c').collapse('show'); }, function () { $('#id22c').collapse('hide'); });
            $("#id23").hover(function () { $('#id23c').collapse('show'); }, function () { $('#id23c').collapse('hide'); });


            $('[data-toggle="tooltip"]').tooltip();
        });

    </script>
</body>
</html>
