﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ContactUs.aspx.cs" Inherits="SSWebsite.ContactUs" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Contact Us | Synaptein Solutions</title>
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="css/style.css" rel="stylesheet" />
</head>
<body class="slider" data-spy="scroll" data-target=".navbar" data-offset="">
      <nav class="navbar navbar-inverse navbar-fixed-top">

        <div class="container-fluid navbar-properties" style="padding-left:120px;padding-right:120px;font-family:'Segoe UI';">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <img src="img/logo.PNG" style="width:250px;" />
            </div>
            <div class=" font-weight-bold collapse navbar-collapse navbar-right" id="myNavbar" style="color:black;">
                <ul class="navbar navbar-nav" id="navbar-text">
                    <li class="btn dropdown active" style="padding:23px;">
                        <strong><a href="/index.html" style="color:white;">Home</a></strong>
                    </li>
                    <li class="btn dropdown" style="padding:23px;">
                        <strong>
                            <a href="/contactus.html" style="color:white;">
                                About Us
                                <span class="caret"></span>
                            </a>
                        </strong>
                        <ul class="dropdown-content list-unstyled text-left list-group" style="color:black;padding:0;">
                            <li class="list-group-item">Certifications</li>
                            <li class="list-group-item">Centers of Excellence</li>
                            <li class="list-group-item">Contract Vehicles</li>
                            <li class="list-group-item">Leadership</li>
                        </ul>
                    </li>
                    <li class="btn dropdown" style="padding:23px;">
                        <strong>
                            <a href="/services.html" style="color:white;">
                                Services
                                <span class="caret"></span>
                            </a>
                        </strong>
                        <ul class="dropdown-content list-unstyled text-left list-group" style="color:black;padding:0;">
                            <li class="list-group-item">Development & Integration</li>
                            <li class="list-group-item">Program & Project Management</li>
                            <li class="list-group-item">>Business Process Consulting</li>
                            <li class="list-group-item">IT Development Support</li>
                            <li class="list-group-item">Business Intelligence and Analytics</li>
                            <li class="list-group-item">JAVA Solutions & Support</li>
                            <li class="list-group-item">Data Management</li>
                            <li class="list-group-item">Information Assurance and Security</li>
                            <li class="list-group-item">Enterprise Risk Management</li>
                        </ul>
                    </li>
                    <li class="btn dropdown" style="padding:23px;">
                        <strong>
                            <a href="/products.html" style="color:white;">
                                Products
                                <span class="caret"></span>
                            </a>
                        </strong>
                        <ul class="dropdown-content list-unstyled text-left list-group" style="color:black;padding:0;">
                            <li class="list-group-item disabled"><strong>Application</strong></li>
                            <li class="list-group-item">Riskspotlight</li>
                            <li class="list-group-item">Apertisoft</li>
                            <li class="list-group-item">ARQ Technology</li>
                            <li class="list-group-item">Purchase Card System</li>
                            <li class="list-group-item disabled"><strong>Mobile</strong></li>
                            <li class="list-group-item">Sitawile</li>
                            <li class="list-group-item">VITwee</li>
                        </ul>
                    </li>
                    <li class="btn dropdown" style="padding:23px;">
                        <strong>
                            <a href="/partners.html" style="color:white;">
                                Partners
                                <span class="caret"></span>
                            </a>
                        </strong>
                        <ul class="dropdown-content list-unstyled text-left list-group" style="color:black;padding:0;">
                            <li class="list-group-item">Microsoft</li>
                            <li class="list-group-item">Redhat</li>
                            <li class="list-group-item">Dell Boomi</li>
                            <li class="list-group-item">rPM3</li>
                            <li class="list-group-item">Riskspotlight</li>
                        </ul>
                    </li>
                    <li class="btn dropdown" style="padding:23px;">
                        <strong>
                            <a href="/career.html" style="color:white;">
                                Careers
                                <span class="caret"></span>
                            </a>
                        </strong>
                        <ul class="dropdown-content list-unstyled text-left list-group" style="color:black;padding:0;">
                            <li class="list-group-item"><a href="career.html">Current Opening</a></li>
                            <li class="list-group-item"><a href="#">Search Jobs</a></li>
                        </ul>
                    </li>
                    <li class="btn dropdown" style="padding:23px;color:white;">
                        <strong>
                            Login
                            <span class="caret"></span>
                        </strong>
                        <ul class="dropdown-content list-unstyled text-left list-group" style="color:black;padding:0;">
                            <li class="list-group-item"><a href="/login.html">HR Login</a></li>
                            <li class="list-group-item"><a href="https://my.adp.com/static/redbox/login.html" target="_blank">Employee Login</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <section>
        <br /><br /><br /><br /><br /><br />
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-4">
                <br /><br /><br />
               <h3 class="text-center text_part2">We're located at:</h3>
                <div class="media">
                    <div class="media-left">
                        <img src="img/US-Flag.png" class="media-object" style="width:60px" />
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading"><strong>Headquarters</strong></h4>
                        <p>4229 Lafayette Center Dr,Suite 1500 Chantilly VA 20151.</p>
                    </div>
                </div>
                <hr />
                <div class="media">
                    <div class="media-left">
                        <img src="img/India-Flag.png" class="media-object" style="width:60px" />
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading"><strong>Business Development Office (India)</strong></h4>
                        <p>Suite #4, Samarpan Talwalker Road,Adarsh lane Malad West Mumbai 400064.</p>
                    </div>
                </div>
                <hr /> 
                <div class="media">
                    <div class="media-left">
                        <img src="img/India-Flag.png" class="media-object" style="width:60px" />
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading"><strong>Development center</strong></h4>
                        <p>916 Ayyappa Society, Madhapur Hyderabad 500081.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <form id="contactus" runat="server">
                <h3 class="text-center text_part2">Your feedback is important to us</h3>
                <div class="form-group">
                    <label for="name">Name:</label>
                    <asp:TextBox ID="name" runat="server" class="form-control" placeholder="e.g. Scott Davis"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label for="email">Email:</label>
                    <asp:TextBox ID="email" runat="server" class="form-control" placeholder="e.g. scott.d@hotmail.com"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label for="email">Phone:</label>
                    <asp:TextBox ID="Phone" runat="server" class="form-control" placeholder="e.g. (940) 541-6289"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label for="reason">Reason:</label>
                    <asp:DropDownList ID="reason" runat="server" class="form-control">
                        <asp:ListItem>General</asp:ListItem>
                        <asp:ListItem>Report a bug on site</asp:ListItem>
                        <asp:ListItem>Technical Support</asp:ListItem>
                        <asp:ListItem>Apply for a position</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="form-group">
                    <label for="comment">Comment:</label>
                    <asp:TextBox ID="comment" runat="server" class="form-control" rows="5" TextMode="MultiLine"></asp:TextBox>
                </div>
                <asp:Button ID="Submit" runat="server" Text="Send Feedback" class="btn btn-primary btn-lg" OnClick="Submit_Click" />
                    <asp:Label ID="lblMessage" runat="server" ForeColor="Blue"></asp:Label>
            </form>
            </div>
            <div class="col-md-2"></div>
            
        </div>
    </section>
    <br /><br /><br />

    <div id="footer">
        <div class="container">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-8 col-sm-6 col-xs-6 footer-custom">Copyrights©Synaptein Solutions 2015.All rights reserved</div>
                <div class="col-md-4 col-sm-6 text-right left-wrapper">
                    <!--      <div class="col-md-6 hidden-sm hidden-xs"> &nbsp;</div>-->

                    <a href="#"><img src="img/facebook.png" /> </a> <a href=""><img src="img/twitter.png" /> </a> <a href=""><img src="img/linkedin.png" /> </a>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
