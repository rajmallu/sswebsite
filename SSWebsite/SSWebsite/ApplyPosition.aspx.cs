﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;

namespace SSWebsite
{
    public partial class ApplyPosition : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            AppliedPosition.Text = "Job ID: " + Request.QueryString["j_id"]+"\n"
                                   +"Title: "+ Request.QueryString["j_title"]+"\n"
                                   +"Location: "+ Request.QueryString["j_location"]+"\n"
                                   + "Summary: " + Request.QueryString["j_summary"] + "\n";
            AppliedPosition.ReadOnly = true;

            //if (PreviousPage != null)
            //{
            //    string j_id = PreviousPage.JOBID;
            //    if (j_id != null)
            //    {
            //        AppliedPosition.Text = j_id;
            //    }
            //    else
            //    {
            //        AppliedPosition.Text = "j_id";
            //    }
            //}
            //else
            //{
            //    AppliedPosition.Text = "prev page";
            //}
            //AppliedPosition.Text = PreviousPage.JOBID+" - "
            //                        + PreviousPage.JOBTITLE+" - "
            //                        + PreviousPage.JOBLOCATION+" <br /> "
            //                        + PreviousPage.JOBSUMMARY;
        }

        protected void UploadDetails_Click(object sender, EventArgs e)
        {

            string user_name = name.Text;
            string user_phone = phone.Text;
            string user_email = Email.Text;
            string user_skype = SkypeID.Text;
            string user_AppliedPosition = "Jr. java developer - 2years - Mc Lean, VA";
            string user_desiredSalary = DesiredSalary.Text;
            string user_ContractType = ContractType.SelectedValue.ToString();
            string user_EmployerDetails = MoreInfo.Text;
            string user_CurrentLocation = CurrentLocation.Text;
            string user_WillingToRelocate = WillingToRelocate.SelectedValue.ToString();
            string user_Availability = Availability.Text;
            string user_EmploymentAuthorization = EmploymentAuthorization.SelectedValue.ToString();
            string user_EmpAuth = EmpAuth.Text;
            string user_Resume_Attachment = Resume.ToString();

            MailMessage mail = new System.Net.Mail.MailMessage();
            mail.To.Add("rajamohan7252@gmail.com");
            mail.From = new System.Net.Mail.MailAddress("rajmallu@outlook.com", "Email head", System.Text.Encoding.UTF8);
            mail.Subject = "Applicant for " + user_AppliedPosition;
            mail.SubjectEncoding = System.Text.Encoding.UTF8;
            mail.Body = "<h3>Application for a position on Synaptein Solutions website</h3><br>" +
                            "Name: <strong>" + user_name +
                            "</strong><br>Contact Number: <strong>" + user_phone +
                            "</strong><br>Email: <strong>" + user_email +
                            "</strong><br>SkypeID: <strong>" + user_skype +
                            "</strong><br>Applied Position: <strong>" + user_AppliedPosition +
                            "</strong><br>Desired Salary: <strong>" + user_desiredSalary +
                            "</strong><br>Contract Type: <strong>" + user_ContractType +
                            "</strong><br>(Employer/Other) Details: <strong>" + user_EmployerDetails +
                            "</strong><br>Current Location: <strong>" + user_CurrentLocation +
                            "</strong><br>Willing To Relocate: <strong>" + user_WillingToRelocate +
                            "</strong><br>Availability : <strong>" + user_Availability +
                            "</strong><br>Employment Authorization: <strong>" + user_EmploymentAuthorization +
                            "</strong><br>Authorization details: <strong>" + user_EmpAuth +
                            "<br><br><a href='mailto:" + user_email + "'>Reply to User</a>";
            mail.BodyEncoding = System.Text.Encoding.UTF8;

            if (Resume.PostedFile != null)
            {
                try
                {
                    string strFileName = System.IO.Path.GetFileName(Resume.PostedFile.FileName);
                    Attachment attachFile = new Attachment(Resume.PostedFile.InputStream, strFileName);
                    mail.Attachments.Add(attachFile);
                }
                catch
                {
                    lblMessage.Text = "Unable to attach the file";
                }
            }

            mail.IsBodyHtml = true;
            mail.Priority = System.Net.Mail.MailPriority.High;
            SmtpClient client = new SmtpClient();
            client.Credentials = new System.Net.NetworkCredential("rajmallu@outlook.com", "raj051117");
            client.Port = 587;
            client.Host = "smtp.outlook.com";
            client.EnableSsl = true;
            try
            {
                client.Send(mail);
                lblMessage.Text = "Application Submitted.";
                name.Text =
                    phone.Text =
                    Email.Text =
                    SkypeID.Text =
                    AppliedPosition.Text =
                    DesiredSalary.Text =

                    MoreInfo.Text =
                    CurrentLocation.Text =

                    Availability.Text =

                    EmpAuth.Text = "";

                WillingToRelocate.SelectedValue = "Yes";
                ContractType.SelectedIndex =

                EmploymentAuthorization.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                lblMessage.Text = "Internal Error! Please contact Adminstrator.";
            }

        }

        protected void ContractType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ContractType.SelectedValue.ToString() == "C2C" || ContractType.SelectedValue.ToString() == "Other")
            {
                MoreInfo.Enabled = true;
                MoreInfo.Focus();
            }
        }

        protected void EmploymentAuthorization_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (EmploymentAuthorization.SelectedValue.ToString() == "Other")
            {
                EmpAuth.Enabled = true;
                EmpAuth.Focus();
            }
        }
    }
}