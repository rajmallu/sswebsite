﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SSWebsite.Secure
{
    public partial class AllJobs : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["New"] != null)
            {
                lblUserLogged.Text = Session["New"].ToString();
            }
            else
            {
                Response.Redirect("~/Login.aspx");
            }
        }

        protected void btnAddJob_Click(object sender, ImageClickEventArgs e)
        {
            Session["value"] = "new";
            Response.Redirect("~/Secure/AddPosition.aspx");
        }

        protected void btnDeleteJob_Click(object sender, ImageClickEventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=rsl-server1.database.windows.net;Initial Catalog=SynapteinSolutionsDB;Persist Security Info=True;User ID=SQL-Admin;Password=Sp33dy0425!");
            string jobID = GV_DisplayAllOpenJobs.SelectedRow.Cells[1].Text;
            try
            {
                string deleteJob = "UPDATE Job SET status='closed' WHERE job_id = '" + jobID + "';";
                con.Open();
                SqlCommand cmd = new SqlCommand(deleteJob, con);
                cmd.ExecuteNonQuery();

                lblMessage.Text = "Record Deleted from Open positions";//

                GV_DisplayAllOpenJobs.DataBind();
                GV_DisplayAllOpenJobs.SelectedIndex = -1;

            }
            catch (Exception ex)
            {
                lblMessage.Text += "Sql Error!" + ex.Message;//
            }
            finally
            {
                con.Close();
            }
        }

        protected void btnEditJob_Click(object sender, ImageClickEventArgs e)
        {
            Session["value"] = GV_DisplayAllOpenJobs.SelectedRow.Cells[1].Text;
            Response.Redirect("AddPosition.aspx");
        }

        protected void btnSearchJob_Click(object sender, EventArgs e)
        {
            
        }

        protected void btnLogout_Click(object sender, ImageClickEventArgs e)
        {
            Session["New"] = null;
            Response.Redirect("~/Login.aspx");
        }

        protected void GV_DisplayAllOpenJobs_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (GV_DisplayAllOpenJobs.SelectedRow.Cells[1].Text != null)
            {
                btnDeleteJob.Enabled = true;
                btnEditJob.Enabled = true;
            }
        }
    }
}