﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SSWebsite.Secure
{
    public partial class AddPosition : System.Web.UI.Page
    {
        //TextBox txtResp;
        //static int i = 0;

        SqlConnection con = new SqlConnection("Data Source=rsl-server1.database.windows.net;Initial Catalog=SynapteinSolutionsDB;Persist Security Info=True;User ID=SQL-Admin;Password=Sp33dy0425!");

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["New"] != null)
            {
                string sessionValue = (string)Session["value"];
                if (sessionValue != "new")
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("select * from Job where job_id='" + sessionValue + "';",con);

                    SqlDataReader dataReader = cmd.ExecuteReader();

                    while (dataReader.Read())
                    {
                        jobid.Text = dataReader.GetString(0);
                        jobid.ReadOnly = true;
                        jobtitle.Text = dataReader.GetString(1);
                        location.Text = dataReader.GetString(2);
                        summary.Text = dataReader.GetString(3);
                        responsibilities.Text = dataReader.GetString(4);
                        requiredskills.Text = dataReader.GetString(5);


                    }
                }
                //lblUserLogged.Text = Session["New"].ToString();
            }
            else
            {
                Response.Redirect("~/Login.aspx");
            }
        }

        protected void submit_Click(object sender, EventArgs e)
        {
            // Modify stored procedure, table
            string jobId = jobid.Text;
            string jobTitle = jobtitle.Text;
            string loc = location.Text;
            string summ = summary.Text;
            string resp = responsibilities.Text;
            string reqSkills = requiredskills.Text;
            string posted_by = Session["New"].ToString();
            string status = "open";


            SqlCommand cmd = new SqlCommand();
            try
            {
                con.Open();
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "spInsertJob";

                cmd.Parameters.Add("@job_id", SqlDbType.VarChar).Value = jobId;
                cmd.Parameters.Add("@title", SqlDbType.VarChar).Value = jobTitle;
                cmd.Parameters.Add("@location", SqlDbType.VarChar).Value = loc;
                cmd.Parameters.Add("@summary", SqlDbType.VarChar).Value = summ;
                cmd.Parameters.Add("@responsibilities", SqlDbType.VarChar).Value = responsibilities;
                cmd.Parameters.Add("@required_skills", SqlDbType.VarChar).Value = reqSkills;
                cmd.Parameters.Add("@posted_by", SqlDbType.VarChar).Value = posted_by;
                cmd.Parameters.Add("@status", SqlDbType.VarChar).Value = status;

                cmd.ExecuteNonQuery();
                lblMessage.Text = "Record Saved successfully";

                jobid.Text =
                    jobtitle.Text =
                    location.Text =
                    summary.Text =
                    requiredskills.Text = "";
            }
            catch (Exception ex)
            {
                lblMessage.Text = "SQL Error!! " + ex.Message;
            }
            finally
            {
                con.Close();
            }
        }
    }
}