﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AllJobs.aspx.cs" Inherits="SSWebsite.Secure.AllJobs" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Synaptein Solutions</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <link href="/css/style.css" rel="stylesheet" />
</head>
<body class="slider" data-spy="scroll" data-target=".navbar" data-offset="">
    <nav class="navbar navbar-light navbar-fixed-top">
        <div class="container-fluid navbar-properties" style="padding-left:120px;padding-right:120px;font-family:'Segoe UI';background-color:white;">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <img src="/img/Logo.png" style="width:150px;height:70px;" />
            </div>
            <div class=" font-weight-bold collapse navbar-collapse navbar-right" id="myNavbar" style="color:black;">
                <ul class="navbar navbar-nav" id="navbar-text">
                    <li class="btn dropdown" style="padding:23px;">
                        <strong><a href="/index.html" style="color:black;font-size:1.3em;">Home</a></strong>
                    </li>
                    <li class="btn dropdown" style="padding:23px;">
                        <strong>
                            <a href="/ContactUs.aspx" style="color:black;font-size:1.3em;">
                                Contact Us
                                <span class="caret"></span>
                            </a>
                        </strong>
                        <ul class="dropdown-content list-unstyled text-left list-group" style="color:black;padding:0;">
                            <li class="list-group-item">Certifications</li>
                            <li class="list-group-item">Centers of Excellence</li>
                            <li class="list-group-item">Contract Vehicles</li>
                            <li class="list-group-item">Leadership</li>
                        </ul>
                    </li>
                    <li class="btn dropdown" style="padding:23px;">
                        <strong>
                            <a href="/services.html" style="color:black;font-size:1.3em;">
                                Services
                                <span class="caret"></span>
                            </a>
                        </strong>
                        <ul class="dropdown-content list-unstyled text-left list-group" style="color:black;padding:0;">
                            <li class="list-group-item">Development & Integration</li>
                            <li class="list-group-item">Program & Project Management</li>
                            <li class="list-group-item">>Business Process Consulting</li>
                            <li class="list-group-item">IT Development Support</li>
                            <li class="list-group-item">Business Intelligence and Analytics</li>
                            <li class="list-group-item">JAVA Solutions & Support</li>
                            <li class="list-group-item">Data Management</li>
                            <li class="list-group-item">Information Assurance and Security</li>
                            <li class="list-group-item">Enterprise Risk Management</li>
                        </ul>
                    </li>
                    <li class="btn dropdown" style="padding:23px;">
                        <strong>
                            <a href="/products.html" style="color:black;font-size:1.3em;">
                                Products
                                <span class="caret"></span>
                            </a>
                        </strong>
                        <ul class="dropdown-content list-unstyled text-left list-group" style="color:black;padding:0;">
                            <li class="list-group-item disabled"><strong>Application</strong></li>
                            <li class="list-group-item">Riskspotlight</li>
                            <li class="list-group-item">Apertisoft</li>
                            <li class="list-group-item">ARQ Technology</li>
                            <li class="list-group-item">Purchase Card System</li>
                            <li class="list-group-item disabled"><strong>Mobile</strong></li>
                            <li class="list-group-item">Sitawile</li>
                            <li class="list-group-item">VITwee</li>
                        </ul>
                    </li>
                    <li class="btn dropdown" style="padding:23px;">
                        <strong>
                            <a href="/partners.html" style="color:black;font-size:1.3em;">
                                Partners
                                <span class="caret"></span>
                            </a>
                        </strong>
                        <ul class="dropdown-content list-unstyled text-left list-group" style="color:black;padding:0;">
                            <li class="list-group-item">Microsoft</li>
                            <li class="list-group-item">Redhat</li>
                            <li class="list-group-item">Dell Boomi</li>
                            <li class="list-group-item">rPM3</li>
                            <li class="list-group-item">Riskspotlight</li>
                        </ul>
                    </li>
                    <li class="btn dropdown" style="padding:23px;">
                        <strong>
                            <a href="/Career.aspx" style="color:black;font-size:1.3em;">
                                Careers
                                <span class="caret"></span>
                            </a>
                        </strong>
                        <ul class="dropdown-content list-unstyled text-left list-group" style="color:black;padding:0;">
                            <li class="list-group-item"><a href="/Career.aspx">Current Opening</a></li>
                            <li class="list-group-item"><a href="#">Search Jobs</a></li>
                        </ul>
                    </li>
                    <li class="btn dropdown" style="padding:23px;color:black;font-size:1.3em;">
                        <strong>
                            Login
                            <span class="caret"></span>
                        </strong>
                        <ul class="dropdown-content list-unstyled text-left list-group" style="color:black;padding:0;">
                            <li class="list-group-item"><a href="/Login.aspx">HR Login</a></li>
                            <li class="list-group-item"><a href="https://my.adp.com/static/redbox/login.html" target="_blank">Employee Login</a></li>
                            <li class="list-group-item"><a href="http://www.riskspotlightportal.com/Home/Index" target="_blank">RiskSpotLight Login</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav> 



    <br />
    <br />
    <br />
    <br />
    <form id="form1" runat="server">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">

                <nav class="navbar navbar-inverse" style="margin: 0;">
                    <div class="container-fluid">
                        <ul class="nav navbar-nav">
                            <li>
                                <a>
                                    <asp:ImageButton ID="btnAddJob" runat="server" class="img-rounded" ImageUrl="/img/new.png" style="width: 35px; height: 30px;"  data-toggle="tooltip" data-placement="bottom" title="Click to add a new position" OnClick="btnAddJob_Click" />
                                </a>
                            </li>
                            <li>
                                <a>
                                    <asp:ImageButton ID="btnDeleteJob" runat="server" ImageUrl="/img/DEL.png" Style="width: 25px; height: 30px;"  data-toggle="tooltip" data-placement="bottom" title="Click to delete the selected position" OnClick="btnDeleteJob_Click" Enabled="False" />
                                </a>
                            </li>
                            <li>
                                <a>
                                    <asp:ImageButton ID="btnEditJob" runat="server" ImageUrl="/img/pencil.png" Style="width: 25px; height: 30px;"  data-toggle="tooltip" data-placement="bottom" title="Click to edit the selected position" OnClick="btnEditJob_Click" Enabled="False" />
                                </a>
                            </li>
                            <li>
                                <a>
                                    <!-- <form class="form-inline"> -->
                                    <div class="input-group">
                                        <asp:TextBox ID="txtSearchJobs" class="form-control" Style="width: 250px;" placeholder="Search Here.." runat="server"></asp:TextBox>
                                        <div class="input-group">
                                            <asp:Button ID="btnSearchJob" class="btn btn-danger" runat="server" Text="Search" OnClick="btnSearchJob_Click" />
                                        </div>
                                    </div>
                                    <!-- </form> -->

                                </a>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a>Logged in as:
                                <asp:Label ID="lblUserLogged" runat="server"></asp:Label></a></li>
                            <li>
                                <a>
                                <asp:ImageButton ID="btnLogout" runat="server" ImageUrl="/img/power-01.ico" Style="width: 40px; height: 40px; padding: 5px;"  data-toggle="tooltip" title="Click to Logout" OnClick="btnLogout_Click" />
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
            <div class="col-md-1"></div>
        </div>
        <div class="row">&nbsp;</div>
        <div class="row">
            <div class="col-md-2"> </div>
            <div class="col-md-8">
 <div class="alert alert-warning alert-dismissable hide" id="friendlyMessage" runat="server">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>
        <!-- <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label> -->

    </strong> 
  </div>
                <asp:Label ID="lblMsg" runat="server" Text="" class="danger"></asp:Label>
            </div>
            <div class="col-md-2"> </div>
        </div>
        <div class="row">&nbsp;</div>
       

        <div class="row">
            <div class="col-md-2">

                <br />

            </div>
            <div class="col-md-8">

                <asp:GridView ID="GV_DisplayAllOpenJobs" runat="server" AutoGenerateColumns="False" AutoGenerateSelectButton="True" CellPadding="4" CellSpacing="5" DataSourceID="SynapteinSolutionsDBConnectionString" Font-Size="Medium" ForeColor="#333333" GridLines="Horizontal" HorizontalAlign="Center" Width="100%" OnSelectedIndexChanged="GV_DisplayAllOpenJobs_SelectedIndexChanged">
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:BoundField DataField="job_id" HeaderText="Job ID" SortExpression="job_id" />
                        <asp:BoundField DataField="title" HeaderText="Job Title" SortExpression="title" />
                        <asp:BoundField DataField="location" HeaderText="Location" SortExpression="location" />
                        <asp:BoundField DataField="posted_by" HeaderText="Posted By" SortExpression="posted_by" />
                        <asp:BoundField DataField="status" HeaderText="Status" SortExpression="status" />
                    </Columns>
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EFF3FB" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
                <asp:SqlDataSource ID="SynapteinSolutionsDBConnectionString" runat="server" ConnectionString="<%$ ConnectionStrings:SynapteinSolutionsDBConStrng %>"
                    SelectCommand="SELECT [job_id], [title], [location], [posted_by], [status] FROM [Job] WHERE [status] = 'open';" FilterExpression="title LIKE '{0}%' OR location LIKE '{0}%' OR posted_by LIKE '{0}%'">
                    <FilterParameters>
                        <asp:ControlParameter ControlID="txtSearchJobs" Name="searchJob" PropertyName="Text" />
                    </FilterParameters>
                </asp:SqlDataSource>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />

                <br />
                <br />


            </div>
            <div class="col-md-2"></div>
        </div>

    </form>
    <div id="footer">
        <div class="container">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-8 col-sm-6 col-xs-6 footer-custom">
                    Copyrights©Synaptein Solutions 2015.All rights reserved;
                </div>

                    <a href="">
                        <img src="img/facebook.png" />
                    </a><a href="">
                        <img src="img/twitter.png" />
                    </a><a href="">
                        <img src="img/linkedin.png" />
                    </a>
                </div>
            </div>
        </div>

    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
</script>
</body>
</html>
