﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;

namespace SSWebsite
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            try
            {
                if (entered_username.Text != "" && entered_password.Text != "")
                {
                    SqlConnection con = new SqlConnection("Data Source=rsl-server1.database.windows.net;Initial Catalog=SynapteinSolutionsDB;Persist Security Info=True;User ID=SQL-Admin;Password=Sp33dy0425!");
                    con.Open();
                    string checkuser = "select count(*) from Employee where username='" + entered_username.Text + "'";
                    SqlCommand cmd = new SqlCommand(checkuser, con);
                    int temp = Convert.ToInt32(cmd.ExecuteScalar().ToString());
                    con.Close();
                    if (temp == 1)
                    {
                        con.Open();
                        string checkPasswordQuery = "select password from Employee where username='" + entered_username.Text + "'";
                        SqlCommand passCmd = new SqlCommand(checkPasswordQuery, con);
                        string password = passCmd.ExecuteScalar().ToString().Replace(" ", "");

                        if (password == entered_password.Text)
                        {
                            lblMessage.Text = "Access Granted!";
                            Session["New"] = entered_username.Text;
                            Response.Redirect("~/Secure/AllJobs.aspx");
                        }

                        else
                        {
                            lblMessage.Text = "Password incorrect. Please try again!";
                        }
                    }
                    else
                    {
                        lblMessage.Text = "User doesn't exist";
                    }
                }
                else
                {
                    lblMessage.Text = "Please enter both Username & Password";
                }
            }
            catch(Exception ex)
            {
                lblMessage.Text = "Login Error! Please contact administrator.";
            }
        }

        protected void SignUp_u_Click(object sender, EventArgs e)
        {
            lblMessage.Text = "Unavailable! Please contact Administrator.";
        }

        protected void ForgotPswd_Click(object sender, EventArgs e)
        {
            lblMessage.Text = "Unavailable! Please contact Technical Support.";
        }

        protected void TechSupport_Click(object sender, EventArgs e)
        {
            Response.Redirect("/ContactUs.aspx?reason=Technical Support");
        }
    }
}