﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Career.aspx.cs" Inherits="SSWebsite.Career" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />

    <meta content="width=device-width, initial-scale=1.0" name="viewport" />

    <meta content="Synaptein Solutions is an equal opportunity employer &amp; prohibits unlawful discrimination based on race, colour, religion, gender, sexual orientation, gender identity/expression, national origin/ancestry, age, disability, marital status." name="description" />

    <title>Careers | Synaptein Solutions</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="js/carousel.js"></script>

    <link href="css/style.css" rel="stylesheet" />
</head>
<body class="slider" data-spy="scroll" data-target=".navbar" data-offset="">

    <nav class="navbar navbar-light navbar-fixed-top">
        <div class="container-fluid navbar-properties" style="padding-left:120px;padding-right:120px;font-family:'Segoe UI';background-color:white;">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <img src="/img/Logo.png" style="width:150px;height:70px;" />
            </div>
            <div class=" font-weight-bold collapse navbar-collapse navbar-right" id="myNavbar" style="color:black;">
                <ul class="navbar navbar-nav" id="navbar-text">
                    <li class="btn dropdown" style="padding:23px;">
                        <strong><a href="/index.html" style="color:black;font-size:1.3em;">Home</a></strong>
                    </li>
                    <li class="btn dropdown" style="padding:23px;">
                        <strong>
                            <a href="/ContactUs.aspx" style="color:black;font-size:1.3em;">
                                Contact Us
                                <span class="caret"></span>
                            </a>
                        </strong>
                        <ul class="dropdown-content list-unstyled text-left list-group" style="color:black;padding:0;">
                            <li class="list-group-item">Certifications</li>
                            <li class="list-group-item">Centers of Excellence</li>
                            <li class="list-group-item">Contract Vehicles</li>
                            <li class="list-group-item">Leadership</li>
                        </ul>
                    </li>
                    <li class="btn dropdown" style="padding:23px;">
                        <strong>
                            <a href="/services.html" style="color:black;font-size:1.3em;">
                                Services
                                <span class="caret"></span>
                            </a>
                        </strong>
                        <ul class="dropdown-content list-unstyled text-left list-group" style="color:black;padding:0;">
                            <li class="list-group-item">Development & Integration</li>
                            <li class="list-group-item">Program & Project Management</li>
                            <li class="list-group-item">>Business Process Consulting</li>
                            <li class="list-group-item">IT Development Support</li>
                            <li class="list-group-item">Business Intelligence and Analytics</li>
                            <li class="list-group-item">JAVA Solutions & Support</li>
                            <li class="list-group-item">Data Management</li>
                            <li class="list-group-item">Information Assurance and Security</li>
                            <li class="list-group-item">Enterprise Risk Management</li>
                        </ul>
                    </li>
                    <li class="btn dropdown" style="padding:23px;">
                        <strong>
                            <a href="/products.html" style="color:black;font-size:1.3em;">
                                Products
                                <span class="caret"></span>
                            </a>
                        </strong>
                        <ul class="dropdown-content list-unstyled text-left list-group" style="color:black;padding:0;">
                            <li class="list-group-item disabled"><strong>Application</strong></li>
                            <li class="list-group-item">Riskspotlight</li>
                            <li class="list-group-item">Apertisoft</li>
                            <li class="list-group-item">ARQ Technology</li>
                            <li class="list-group-item">Purchase Card System</li>
                            <li class="list-group-item disabled"><strong>Mobile</strong></li>
                            <li class="list-group-item">Sitawile</li>
                            <li class="list-group-item">VITwee</li>
                        </ul>
                    </li>
                    <li class="btn dropdown" style="padding:23px;">
                        <strong>
                            <a href="/partners.html" style="color:black;font-size:1.3em;">
                                Partners
                                <span class="caret"></span>
                            </a>
                        </strong>
                        <ul class="dropdown-content list-unstyled text-left list-group" style="color:black;padding:0;">
                            <li class="list-group-item">Microsoft</li>
                            <li class="list-group-item">Redhat</li>
                            <li class="list-group-item">Dell Boomi</li>
                            <li class="list-group-item">rPM3</li>
                            <li class="list-group-item">Riskspotlight</li>
                        </ul>
                    </li>
                    <li class="btn dropdown" style="padding:23px;">
                        <strong>
                            <a href="/Career.aspx" style="color:black;font-size:1.3em;">
                                Careers
                                <span class="caret"></span>
                            </a>
                        </strong>
                        <ul class="dropdown-content list-unstyled text-left list-group" style="color:black;padding:0;">
                            <li class="list-group-item"><a href="/Career.aspx">Current Opening</a></li>
                            <li class="list-group-item"><a href="#">Search Jobs</a></li>
                        </ul>
                    </li>
                    <li class="btn dropdown" style="padding:23px;color:black;font-size:1.3em;">
                        <strong>
                            Login
                            <span class="caret"></span>
                        </strong>
                        <ul class="dropdown-content list-unstyled text-left list-group" style="color:black;padding:0;">
                            <li class="list-group-item"><a href="/Login.aspx">HR Login</a></li>
                            <li class="list-group-item"><a href="https://my.adp.com/static/redbox/login.html" target="_blank">Employee Login</a></li>
                            <li class="list-group-item"><a href="http://www.riskspotlightportal.com/Home/Index" target="_blank">RiskSpotLight Login</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav> 

    <div id="wrapper" style="display:none;background-color:lightgray;">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li><a href="index.html">HOME</a></li>
                <li><a href="services.html">SERVICES</a></li>
                <li><a href="products.html">PRODUCTS</a></li>
                <li><a href="partners.html">PARTNERS &amp; CLIENTS</a></li>
                <li class="active"><a href="Career.aspx">CAREERS</a></li>
            </ul>

        </div>

        <!-- /#sidebar-wrapper -->
    </div>

    <!-- /#wrapper -->



    <div class="container-fluid" style="background-color:lightgray;">

        <section class="container">

            <section class="col-md-12 career-margin clearfix">

                <section class="col-md-3">

                    <h2 class="career_display"><a data-toggle="pill" href="#career">Careers </a></h2>



                    <ul class="nav nav-pills nav-stacked career_list" role="navigation">

                        <li role="presentation"><a data-toggle="pill" href="#jobpost">Search Jobs and Apply </a></li>

                        <li role="presentation"><a data-toggle="pill" href="#benefits">Benefits</a></li>

                        <li role="presentation"><a data-toggle="pill" href="#policies">Employment Policies</a></li>

                    </ul>

                </section>



                <section class="col-md-9 tab-content">

                    <section class="tab-pane fade in active" id="career">

                        <h2 class="btm_spaceNone">Careers</h2>



                        <p class="border">&nbsp;</p>



                        <h2>An Equal Opportunity Employer</h2>



                        <p>Synaptein Solutions is committed to the principles of equal employment opportunity. It is the policy of Synaptein Solutions to recruit, hire, and promote for all positions and to administer all Human Resource matters such as wages, benefits, assignments, promotions, and transfers and all other terms and conditions of employment without regard to race, color, religion, creed, sex, national origin, citizenship status, ancestry, age, disability, sexual orientation, gender identity or expression, marital status, genetic disposition, veteran status, or any other basis protected by federal, state, and/or local law.</p>



                        <h2>Applicants with Disabilities</h2>



                        <p>Synapteinn  Solutions is committed to working with and providing reasonable accommodation to individuals with disabilities. We will provide reasonable accommodations to applicants with qualified disabilities in accordance with the Americans with Disabilities Act. If you require a reasonable accommodation for our application process, please contact us <b><a>accommodationsforapp@synapteinsolutions.com</a> </b>. Requests will be kept confidential and shared strictly on a need-to-know basis.</p>

                    </section>



                    <section class="tab-pane fade" id="jobpost">
                        <h2 class="btm_spaceNone">Search Jobs and Apply</h2>
                        <p>Below is a listing of our open positions.</p>
                        <asp:Label ID="lblMessage" runat="server" class="danger" Text=""></asp:Label>
                        <ul class="job-details"  id="jobsList" runat="server">
                            
                        </ul>
                    </section>
                    
                    
                    <section class="tab-pane fade" id="benefits">
                        <h2 class="btm_spaceNone">Benefits</h2>



                        <p>Synaptein Solutions employees enjoy competitive salaries and generous benefits including:</p>



                        <ul>

                            <li>Comprehensive medical, dental, vision and life insurance coverage</li>

                            <li>Retirement savings plan with a generous employer contribution</li>

                            <li>Three weeks of paid time off</li>

                            <li>Short-term and long-term disability coverage</li>

                            <li>Training and talent management and development</li>

                        </ul>
                        <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
                    </section>



                    <section class="tab-pane fade" id="policies">

                        <h2 class="btm_spaceNone">Employment Policies</h2>

                        Synaptein Solutions is dedicated to the principle of equal opportunity and its programs, services and employment policies are guided by that principle. Any offer of employment at Synaptein Solutions is contingent upon the satisfactory completion of background and reference checks.
                        <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
                    </section>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                </section>
            </section>
        </section>
    </div>


    <div id="footer">
        <div class="container">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-8 col-sm-6 col-xs-6 footer-custom">Copyrights©Synaptein Solutions 2015.All rights reserved</div>
                <div class="col-md-4 col-sm-6 text-right left-wrapper">
                    <!--      <div class="col-md-6 hidden-sm hidden-xs"> &nbsp;</div>-->

                    <a href=""><img src="img/facebook.png" /> </a> <a href=""><img src="img/twitter.png" /> </a> <a href=""><img src="img/linkedin.png" /> </a>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
