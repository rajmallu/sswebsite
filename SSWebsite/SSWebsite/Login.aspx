﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="SSWebsite.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <title>Login | Synaptein Solutions</title>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="IT Solutions provider for services like Data Warehousing, Business Process Management, Quality Assurance and more. Get in touch with us today and take your business to new heights today." />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="js/carousel.js"></script>
    <link href="css/style.css" rel="stylesheet" />
    <style>
        section {
            position: relative;
        }
        #loginForm {
            height: 63%;
        }
    </style>

</head>
<body class="slider" data-spy="scroll" data-target=".navbar" data-offset="" style="background-color:lightgray;">
 <nav class="navbar navbar-light navbar-fixed-top">
        <div class="container-fluid navbar-properties" style="padding-left:120px;padding-right:120px;font-family:'Segoe UI';background-color:white;">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <img src="/img/Logo.png" style="width:150px;height:70px;" />
            </div>
            <div class=" font-weight-bold collapse navbar-collapse navbar-right" id="myNavbar" style="color:black;">
                <ul class="navbar navbar-nav" id="navbar-text">
                    <li class="btn dropdown" style="padding:23px;">
                        <strong><a href="/index.html" style="color:black;font-size:1.3em;">Home</a></strong>
                    </li>
                    <li class="btn dropdown" style="padding:23px;">
                        <strong>
                            <a href="/ContactUs.aspx" style="color:black;font-size:1.3em;">
                                Contact Us
                                <span class="caret"></span>
                            </a>
                        </strong>
                        <ul class="dropdown-content list-unstyled text-left list-group" style="color:black;padding:0;">
                            <li class="list-group-item">Certifications</li>
                            <li class="list-group-item">Centers of Excellence</li>
                            <li class="list-group-item">Contract Vehicles</li>
                            <li class="list-group-item">Leadership</li>
                        </ul>
                    </li>
                    <li class="btn dropdown" style="padding:23px;">
                        <strong>
                            <a href="/services.html" style="color:black;font-size:1.3em;">
                                Services
                                <span class="caret"></span>
                            </a>
                        </strong>
                        <ul class="dropdown-content list-unstyled text-left list-group" style="color:black;padding:0;">
                            <li class="list-group-item">Development & Integration</li>
                            <li class="list-group-item">Program & Project Management</li>
                            <li class="list-group-item">>Business Process Consulting</li>
                            <li class="list-group-item">IT Development Support</li>
                            <li class="list-group-item">Business Intelligence and Analytics</li>
                            <li class="list-group-item">JAVA Solutions & Support</li>
                            <li class="list-group-item">Data Management</li>
                            <li class="list-group-item">Information Assurance and Security</li>
                            <li class="list-group-item">Enterprise Risk Management</li>
                        </ul>
                    </li>
                    <li class="btn dropdown" style="padding:23px;">
                        <strong>
                            <a href="/products.html" style="color:black;font-size:1.3em;">
                                Products
                                <span class="caret"></span>
                            </a>
                        </strong>
                        <ul class="dropdown-content list-unstyled text-left list-group" style="color:black;padding:0;">
                            <li class="list-group-item disabled"><strong>Application</strong></li>
                            <li class="list-group-item">Riskspotlight</li>
                            <li class="list-group-item">Apertisoft</li>
                            <li class="list-group-item">ARQ Technology</li>
                            <li class="list-group-item">Purchase Card System</li>
                            <li class="list-group-item disabled"><strong>Mobile</strong></li>
                            <li class="list-group-item">Sitawile</li>
                            <li class="list-group-item">VITwee</li>
                        </ul>
                    </li>
                    <li class="btn dropdown" style="padding:23px;">
                        <strong>
                            <a href="/partners.html" style="color:black;font-size:1.3em;">
                                Partners
                                <span class="caret"></span>
                            </a>
                        </strong>
                        <ul class="dropdown-content list-unstyled text-left list-group" style="color:black;padding:0;">
                            <li class="list-group-item">Microsoft</li>
                            <li class="list-group-item">Redhat</li>
                            <li class="list-group-item">Dell Boomi</li>
                            <li class="list-group-item">rPM3</li>
                            <li class="list-group-item">Riskspotlight</li>
                        </ul>
                    </li>
                    <li class="btn dropdown" style="padding:23px;">
                        <strong>
                            <a href="/Career.aspx" style="color:black;font-size:1.3em;">
                                Careers
                                <span class="caret"></span>
                            </a>
                        </strong>
                        <ul class="dropdown-content list-unstyled text-left list-group" style="color:black;padding:0;">
                            <li class="list-group-item"><a href="/Career.aspx">Current Opening</a></li>
                            <li class="list-group-item"><a href="#">Search Jobs</a></li>
                        </ul>
                    </li>
                    <li class="btn dropdown" style="padding:23px;color:black;font-size:1.3em;">
                        <strong>
                            Login
                            <span class="caret"></span>
                        </strong>
                        <ul class="dropdown-content list-unstyled text-left list-group" style="color:black;padding:0;">
                            <li class="list-group-item"><a href="/Login.aspx">HR Login</a></li>
                            <li class="list-group-item"><a href="https://my.adp.com/static/redbox/login.html" target="_blank">Employee Login</a></li>
                            <li class="list-group-item"><a href="http://www.riskspotlightportal.com/Home/Index" target="_blank">RiskSpotLight Login</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav> 


    <section data-spy="scroll" data-target=".navbar" data-offset="50">
        <div class="container">
            <div class="row container-fluid" style="height:20%;">&nbsp;
            <br /> <br /><br /> <br /><br /> <br />
           </div>
            <div class="row" id="loginForm">
                <div class="col-lg-4"></div>
                <div class="col-lg-4 bg-success" style="padding-top:2%;border-radius:5px;height:90%;">
                    <form action="#" method="post" class="text-center" runat="server">
                        <h3>Login</h3><br />
                        <div class="input-group" style="padding:5px;">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user" style="font-size:0.75em;"></i></span>
                            <asp:TextBox ID="entered_username" runat="server" class="form-control" placeholder="E-Mail" style="background-color:lightgray;"></asp:TextBox>
                            <br />
                        </div>
                        <div class="input-group" style="padding:5px;">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock" style="font-size:0.75em;"></i></span>
                            <asp:TextBox ID="entered_password" runat="server" class="form-control" style="background-color:lightgray;" placeholder="Password" TextMode="Password"></asp:TextBox>
                            <br />
                        </div>
                        <div class="text-right text-danger" style="font-family:'Segoe UI'">
                            <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label></div><br />
                        <asp:Button ID="Submit" runat="server" Text="Login" class="btn btn-more btn-info" style="width:150px;" OnClick="Submit_Click" />
                        <br />
                        <br />
                        <asp:LinkButton ID="ForgotPswd" runat="server" OnClick="ForgotPswd_Click">Forgot Password?</asp:LinkButton>
                        &emsp;|&emsp;
                        <asp:LinkButton ID="TechSupport" runat="server" OnClick="TechSupport_Click">Technical Support</asp:LinkButton>
                        <br />
                        <br />
                        New Member?<asp:LinkButton ID="SignUp_u" runat="server" OnClick="SignUp_u_Click">Sign Up</asp:LinkButton>
                        <br /><br /> <br />
                    </form>
                </div>
                <div class="col-lg-4"></div>

            </div>
            <br /><br /><br /> <br /><br />
       
        </div>
        <div id="footer">
            <div class="container">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="col-md-8 col-sm-6 col-xs-6 footer-custom">Copyrights©Synaptein Solutions 2015.All rights reserved</div>
                    <div class="col-md-4 col-sm-6 text-right left-wrapper">
                        <!--      <div class="col-md-6 hidden-sm hidden-xs"> &nbsp;</div>-->
                        <a href="#"><img src="img/facebook.png" /> </a> 
                        <a href="#"><img src="img/twitter.png" /> </a> 
                        <a href="#"><img src="img/linkedin.png" /> </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>

</body>
</html>
